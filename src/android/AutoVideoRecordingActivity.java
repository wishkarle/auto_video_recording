package com.wishkarle.mediacapture;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.coremedia.iso.IsoFile;
import com.coremedia.iso.boxes.Container;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.coremedia.iso.boxes.TrackBox;
import com.googlecode.mp4parser.DataSource;
import com.googlecode.mp4parser.FileDataSourceImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Mp4TrackImpl;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import com.utwiz.cg112.R;


public class AutoVideoRecordingActivity extends Activity {

        private Size mPreviewSize;
        private Size mVideoSize;
        private MediaRecorder mMediaRecorder;
        public boolean mIsRecordingVideo;
        private HandlerThread mBackgroundThread;
        private Handler mBackgroundHandler;
        private Semaphore mCameraOpenCloseLock = new Semaphore(1);
        private static final String TAG = "CameraFragment";
        private static final String VIDEO_DIRECTORY_NAME = "CG112";
        AutoFitTextureView mTextureView;
        Button mRecordVideo;
        TextView mCountdown;
        int counter = 8;
        Chronometer chronometer;
        private String mOutputFilePath;
        private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;
        private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
        private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();
        private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
        private File mCurrentFile;
        private CameraDevice mCameraDevice;
        private CameraCaptureSession mPreviewSession;

        static {
            INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 0);
            INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
            INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 180);
            INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
        }

        static {
            DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
            DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
            DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);
            DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
        }


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_auto_video_recording);
            //getSupportActionBar().hide();
            mTextureView = findViewById(R.id.mTextureView);
            mRecordVideo = findViewById(R.id.mRecordVideo);
            chronometer = (Chronometer) findViewById(R.id.chronometer);
            mRecordVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mIsRecordingVideo) {
                        try {
                            stopRecordingVideo();
                            //prepareViews();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        startRecordingVideo();
                        mRecordVideo.setText(R.string.stop_recording);
                        //Receive out put file here
                        mOutputFilePath = getCurrentFile().getAbsolutePath();
                    }
                }
            });
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        stopRecordingVideo();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, 11000);
        }



        private String parseVideo(String mFilePath) throws IOException {
            DataSource channel = new FileDataSourceImpl(mFilePath);
            IsoFile isoFile = new IsoFile(channel);
            List<TrackBox> trackBoxes = isoFile.getMovieBox().getBoxes(TrackBox.class);
            boolean isError = false;
            for (TrackBox trackBox : trackBoxes) {
                TimeToSampleBox.Entry firstEntry = trackBox.getMediaBox().getMediaInformationBox().getSampleTableBox().getTimeToSampleBox().getEntries().get(0);
                // Detect if first sample is a problem and fix it in isoFile
                // This is a hack. The audio deltas are 1024 for my files, and video deltas about 3000
                // 10000 seems sufficient since for 30 fps the normal delta is about 3000
                if (firstEntry.getDelta() > 10000) {
                    isError = true;
                    firstEntry.setDelta(3000);
                }
            }
            File file = getOutputMediaFile();
            String filePath = file.getAbsolutePath();
            if (isError) {
                Movie movie = new Movie();
                for (TrackBox trackBox : trackBoxes) {
                    movie.addTrack(new Mp4TrackImpl(channel.toString() + "[" + trackBox.getTrackHeaderBox().getTrackId() + "]", trackBox));
                }
                movie.setMatrix(isoFile.getMovieBox().getMovieHeaderBox().getMatrix());
                Container out = new DefaultMp4Builder().build(movie);

                //delete file first!
                FileChannel fc = new RandomAccessFile(filePath, "rw").getChannel();
                out.writeContainer(fc);
                fc.close();
                Log.d(TAG, "Finished correcting raw video");
                return filePath;
            }
            return mFilePath;
        }

        /**
         * Create directory and return file
         * returning video file
         */
        private File getOutputMediaFile() {
            // External sdcard file location
            File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
                    VIDEO_DIRECTORY_NAME);
            // Create storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d(TAG, "Oops! Failed create "
                            + VIDEO_DIRECTORY_NAME + " directory");
                    return null;
                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            File mediaFile;

            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
            return mediaFile;
        }




        private TextureView.SurfaceTextureListener mSurfaceTextureListener
                = new TextureView.SurfaceTextureListener() {

            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture,
                                                  int width, int height) {
                openCamera(width, height);
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture,
                                                    int width, int height) {
                configureTransform(width, height);
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
                return true;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
            }

        };



        /**
         * {@link CameraDevice.StateCallback} is called when {@link CameraDevice} changes its status.
         */
        private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

            @Override
            public void onOpened(@NonNull CameraDevice cameraDevice) {
                mCameraDevice = cameraDevice;
                startPreview();
                mCameraOpenCloseLock.release();
                if (null != mTextureView) {
                    configureTransform(mTextureView.getWidth(), mTextureView.getHeight());
                    if (mIsRecordingVideo) {
                        try {
                            stopRecordingVideo();
                            //prepareViews();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        startRecordingVideo();
                        mRecordVideo.setText(R.string.stop_recording);
                        //Receive out put file here
                        mOutputFilePath = getCurrentFile().getAbsolutePath();
                    }
                }
            }

            @Override
            public void onDisconnected(@NonNull CameraDevice cameraDevice) {
                mCameraOpenCloseLock.release();
                cameraDevice.close();
                mCameraDevice = null;
            }

            @Override
            public void onError(@NonNull CameraDevice cameraDevice, int error) {
                mCameraOpenCloseLock.release();
                cameraDevice.close();
                mCameraDevice = null;
                Activity activity = AutoVideoRecordingActivity.this;
                if (null != activity) {
                    activity.finish();
                }
            }

        };
        private Integer mSensorOrientation;
        private CaptureRequest.Builder mPreviewBuilder;


        /**
         * In this sample, we choose a video size with 3x4 for  aspect ratio. for more perfectness 720 as well Also, we don't use sizes
         * larger than 1080p, since MediaRecorder cannot handle such a high-resolution video.
         *
         * @param choices The list of available sizes
         * @return The video size 1080p,720px
         */
        private static Size chooseVideoSize(Size[] choices) {
            for (Size size : choices) {
                if (1920 == size.getWidth() && 1080 == size.getHeight()) {
                    return size;
                }
            }
            for (Size size : choices) {
                if (size.getWidth() == size.getHeight() * 4 / 3 && size.getWidth() <= 1080) {
                    return size;
                }
            }
            Log.e(TAG, "Couldn't find any suitable video size");
            return choices[choices.length - 1];
        }


        /**
         * Given {@code choices} of {@code Size}s supported by a camera, chooses the smallest one whose
         * width and height are at least as large as the respective requested values, and whose aspect
         * ratio matches with the specified value.
         *
         * @param choices     The list of sizes that the camera supports for the intended output class
         * @param width       The minimum desired width
         * @param height      The minimum desired height
         * @param aspectRatio The aspect ratio
         * @return The optimal {@code Size}, or an arbitrary one if none were big enough
         */
        private static Size chooseOptimalSize(Size[] choices, int width, int height, Size aspectRatio) {
            // Collect the supported resolutions that are at least as big as the preview Surface
            List<Size> bigEnough = new ArrayList<>();
            int w = aspectRatio.getWidth();
            int h = aspectRatio.getHeight();
            for (Size option : choices) {
                if (option.getHeight() == option.getWidth() * h / w &&
                        option.getWidth() >= width && option.getHeight() >= height) {
                    bigEnough.add(option);
                }
            }

            // Pick the smallest of those, assuming we found any
            if (bigEnough.size() > 0) {
                return Collections.min(bigEnough, new CompareSizesByArea());
            } else {
                Log.e(TAG, "Couldn't find any suitable preview size");
                return choices[0];
            }
        }


        @Override
        public void onResume() {
            super.onResume();
            startBackgroundThread();
            requestPermission();
        }

        @Override
        public void onPause() {
            closeCamera();
            stopBackgroundThread();
            super.onPause();
        }

        protected File getCurrentFile() {
            return mCurrentFile;
        }

        /**
         * Starts a background thread and its {@link Handler}.
         */
        private void startBackgroundThread() {
            mBackgroundThread = new HandlerThread("CameraBackground");
            mBackgroundThread.start();
            mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
        }

        /**
         * Stops the background thread and its {@link Handler}.
         */
        private void stopBackgroundThread() {
            mBackgroundThread.quitSafely();
            try {
                mBackgroundThread.join();
                mBackgroundThread = null;
                mBackgroundHandler = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        /**
         * Requesting permissions storage, audio and camera at once
         */
        public void requestPermission() {
            Dexter.withActivity(AutoVideoRecordingActivity.this).withPermissions(Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            // check if all permissions are granted or not
                            if (report.areAllPermissionsGranted()) {
                                if (mTextureView.isAvailable()) {
                                    openCamera(mTextureView.getWidth(), mTextureView.getHeight());
                                } else {
                                    mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
                                }
                            }
                            // check for permanent denial of any permission show alert dialog
                            if (report.isAnyPermissionPermanentlyDenied()) {
                                // open Settings activity
                                showSettingsDialog();
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).withErrorListener(error -> Toast.makeText(AutoVideoRecordingActivity.this.getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                    .onSameThread()
                    .check();
        }

        /**
         * Showing Alert Dialog with Settings option in case of deny any permission
         */
        private void showSettingsDialog() {
            AlertDialog.Builder builder = new AlertDialog.Builder(AutoVideoRecordingActivity.this);
            builder.setTitle(getString(R.string.message_need_permission));
            builder.setMessage(getString(R.string.message_permission));
            builder.setPositiveButton(getString(R.string.title_go_to_setting), (dialog, which) -> {
                dialog.cancel();
                openSettings();
            });
            builder.show();

        }


        // navigating settings app
        private void openSettings() {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", AutoVideoRecordingActivity.this.getPackageName(), null);
            intent.setData(uri);
            startActivityForResult(intent, 101);
        }

        /**
         * Tries to open a {@link CameraDevice}. The result is listened by `mStateCallback`.
         */
        private void openCamera(int width, int height) {
            final Activity activity = AutoVideoRecordingActivity.this;
            if (null == activity || activity.isFinishing()) {
                return;
            }
            CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
            try {
                Log.d(TAG, "tryAcquire");
                if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                    throw new RuntimeException("Time out waiting to lock camera opening.");
                }
                /**
                 * default front camera will activate
                 */
                String cameraId = manager.getCameraIdList()[0];

                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
                StreamConfigurationMap map = characteristics
                        .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                if (map == null) {
                    throw new RuntimeException("Cannot get available preview/video sizes");
                }
                mVideoSize = chooseVideoSize(map.getOutputSizes(MediaRecorder.class));
                mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                        width, height, mVideoSize);

                int orientation = getResources().getConfiguration().orientation;
                if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    mTextureView.setAspectRatio(mPreviewSize.getWidth(), mPreviewSize.getHeight());
                } else {
                    mTextureView.setAspectRatio(mPreviewSize.getHeight(), mPreviewSize.getWidth());
                }
                configureTransform(width, height);
                mMediaRecorder = new MediaRecorder();
                if (ActivityCompat.checkSelfPermission(AutoVideoRecordingActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    requestPermission();
                    return;
                }
                manager.openCamera(cameraId, mStateCallback, null);
            } catch (CameraAccessException e) {
                Log.e(TAG, "openCamera: Cannot access the camera.");
            } catch (NullPointerException e) {
                Log.e(TAG, "Camera2API is not supported on the device.");
            } catch (InterruptedException e) {
                throw new RuntimeException("Interrupted while trying to lock camera opening.");
            }
        }



        /**
         * close camera and release object
         */
        private void closeCamera() {
            try {
                mCameraOpenCloseLock.acquire();
                closePreviewSession();
                if (null != mCameraDevice) {
                    mCameraDevice.close();
                    mCameraDevice = null;
                }
                if (null != mMediaRecorder) {
                    mMediaRecorder.release();
                    mMediaRecorder = null;
                }
            } catch (InterruptedException e) {
                throw new RuntimeException("Interrupted while trying to lock camera closing.");
            } finally {
                mCameraOpenCloseLock.release();
            }
        }

        /**
         * Start the camera preview.
         */
        private void startPreview() {
            if (null == mCameraDevice || !mTextureView.isAvailable() || null == mPreviewSize) {
                return;
            }
            try {
                closePreviewSession();
                SurfaceTexture texture = mTextureView.getSurfaceTexture();
                assert texture != null;
                texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
                mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                Surface previewSurface = new Surface(texture);
                mPreviewBuilder.addTarget(previewSurface);
                mCameraDevice.createCaptureSession(Collections.singletonList(previewSurface),
                        new CameraCaptureSession.StateCallback() {

                            @Override
                            public void onConfigured(@NonNull CameraCaptureSession session) {
                                mPreviewSession = session;
                                //updatePreview();
                            }

                            @Override
                            public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                                Log.e(TAG, "onConfigureFailed: Failed ");
                            }
                        }, mBackgroundHandler);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

        /**
         * Update the camera preview. {@link #startPreview()} needs to be called in advance.
         */
        private void updatePreview() {
            if (null == mCameraDevice) {
                return;
            }
            try {
                setUpCaptureRequestBuilder(mPreviewBuilder);
                HandlerThread thread = new HandlerThread("CameraPreview");
                thread.start();
                mPreviewSession.setRepeatingRequest(mPreviewBuilder.build(), null, mBackgroundHandler);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

        private void setUpCaptureRequestBuilder(CaptureRequest.Builder builder) {
            builder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        }

        /**
         * Configures the necessary {@link Matrix} transformation to `mTextureView`.
         * This method should not to be called until the camera preview size is determined in
         * openCamera, or until the size of `mTextureView` is fixed.
         *
         * @param viewWidth  The width of `mTextureView`
         * @param viewHeight The height of `mTextureView`
         */
        private void configureTransform(int viewWidth, int viewHeight) {
            Activity activity = AutoVideoRecordingActivity.this;
            if (null == mTextureView || null == mPreviewSize || null == activity) {
                return;
            }
            int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
            Matrix matrix = new Matrix();
            RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
            RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
            float centerX = viewRect.centerX();
            float centerY = viewRect.centerY();
            if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
                bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
                matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
                float scale = Math.max(
                        (float) viewHeight / mPreviewSize.getHeight(),
                        (float) viewWidth / mPreviewSize.getWidth());
                matrix.postScale(scale, scale, centerX, centerY);
                matrix.postRotate(90 * (rotation - 2), centerX, centerY);
            }
            mTextureView.setTransform(matrix);
        }

        private void setUpMediaRecorder() throws IOException {
            final Activity activity = AutoVideoRecordingActivity.this;
            if (null == activity) {
                return;
            }
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            /**
             * create video output file
             */
            mCurrentFile = getOutputMediaFile();
            /**
             * set output file in media recorder
             */
            mMediaRecorder.setOutputFile(mCurrentFile.getAbsolutePath());
            CamcorderProfile profile = CamcorderProfile.get(CamcorderProfile.QUALITY_480P);
            mMediaRecorder.setVideoFrameRate(profile.videoFrameRate);
            mMediaRecorder.setVideoSize(profile.videoFrameWidth, profile.videoFrameHeight);
            mMediaRecorder.setVideoEncodingBitRate(profile.videoBitRate);
            mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mMediaRecorder.setAudioEncodingBitRate(profile.audioBitRate);
            mMediaRecorder.setAudioSamplingRate(profile.audioSampleRate);



            int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
            switch (mSensorOrientation) {
                case SENSOR_ORIENTATION_DEFAULT_DEGREES:
                    mMediaRecorder.setOrientationHint(DEFAULT_ORIENTATIONS.get(rotation));
                    break;
                case SENSOR_ORIENTATION_INVERSE_DEGREES:
                    mMediaRecorder.setOrientationHint(INVERSE_ORIENTATIONS.get(rotation));
                    break;
            }
            mMediaRecorder.prepare();
        }

        public void startRecordingVideo() {
            if (null == mCameraDevice || !mTextureView.isAvailable() || null == mPreviewSize) {
                return;
            }
            try {
                closePreviewSession();
                setUpMediaRecorder();
                SurfaceTexture texture = mTextureView.getSurfaceTexture();
                assert texture != null;
                texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
                mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
                List<Surface> surfaces = new ArrayList<>();

                /**
                 * Surface for the camera preview set up
                 */

                Surface previewSurface = new Surface(texture);
                surfaces.add(previewSurface);
                mPreviewBuilder.addTarget(previewSurface);

                //MediaRecorder setup for surface
                Surface recorderSurface = mMediaRecorder.getSurface();
                surfaces.add(recorderSurface);
                mPreviewBuilder.addTarget(recorderSurface);

                // Start a capture session
                mCameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {

                    @Override
                    public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                        mPreviewSession = cameraCaptureSession;
                        updatePreview();
                        AutoVideoRecordingActivity.this.runOnUiThread(() -> {
                            mIsRecordingVideo = true;
                            // Start recording

                            mMediaRecorder.start();
                            chronometer.setBase(SystemClock.elapsedRealtime());
                            chronometer.setVisibility(View.VISIBLE);
                            chronometer.start();


                           /* new CountDownTimer(8000,1000) {
                                @Override
                                public void onTick(long millisUntilFinished) {
                                    mCountdown.setText(String.valueOf(counter));
                                    counter--;
                                }
                                @Override
                                public void onFinish() {
                                    try {
                                        stopRecordingVideo();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    mCountdown.setText("Finished");
                                }
                            }.start();*/
                         /*   new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        stopRecordingVideo();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, 8000);*/
                        });
                    }

                    @Override
                    public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                        Log.e(TAG, "onConfigureFailed: Failed");
                    }
                }, mBackgroundHandler);
            } catch (CameraAccessException | IOException e) {
                e.printStackTrace();
            }

        }

        private void closePreviewSession() {
            if (mPreviewSession != null) {
                mPreviewSession.close();
                mPreviewSession = null;
            }
        }

        public void stopRecordingVideo() throws Exception {
            // UI
            mIsRecordingVideo = false;
            chronometer.setVisibility(View.INVISIBLE);
            chronometer.stop();
            try {
                mPreviewSession.stopRepeating();
                mPreviewSession.abortCaptures();
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }

            // Stop recording
            mMediaRecorder.stop();
            mMediaRecorder.reset();
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result",mOutputFilePath);
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }

        /**
         * Compares two {@code Size}s based on their areas.
         */
        static class CompareSizesByArea implements Comparator<Size> {

            @Override
            public int compare(Size lhs, Size rhs) {
                // We cast here to ensure the multiplications won't overflow
                return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                        (long) rhs.getWidth() * rhs.getHeight());
            }

        }

}
